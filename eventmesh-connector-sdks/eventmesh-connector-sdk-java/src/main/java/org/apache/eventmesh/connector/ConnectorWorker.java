package org.apache.eventmesh.connector;

public interface ConnectorWorker {
    void start();

    void stop();
}
